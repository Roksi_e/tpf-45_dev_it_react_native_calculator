import { StatusBar } from 'expo-status-bar'
import { MainScreen } from './src/screen/MainScreen'
import { CalcState } from './src/context/CalcState'
import { SafeAreaView, StyleSheet } from 'react-native'

export default function App() {
	return (
		<CalcState>
			<SafeAreaView style={styles.container}>
				<MainScreen />
			</SafeAreaView>
			<StatusBar style="auto" />
		</CalcState>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	}
})
