export const validator = (value, pattern) => {
	return pattern.test(value)
}

export const calculate = (a, b, operator) => {
	if (b === '') {
		b = a
		operator = '+'
	}
	switch (operator) {
		case '+':
			return parseFloat(a) + parseFloat(b)

		case '-':
			return parseFloat(a) - parseFloat(b)

		case '×':
			return parseFloat(a) * parseFloat(b)
		case '%':
			return parseFloat(a) / 100

		case '÷':
			return b !== '0' ? parseFloat(a) / parseFloat(b) : 'Error'

		default:
			return
	}
}
