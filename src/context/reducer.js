import { BTN_VALUE, CLEAR_BTN, OPERATOR_KEYS, OPERATOR_TYPES, OPERATOR_VALUE, RESULT_VALUE } from './types'
import { validator, calculate } from '../methods'

const handlers = {
	[BTN_VALUE]: (state, { value }) => {
		if (state.operator === '' && state.num2 === '' && validator(value, /[0-9.]/)) {
			if (state.result !== '' && state.memory === '') {
				return {
					...state,
					num1: value,
					firstDisplay: value,
					secondDisplay: '',
					result: ''
				}
			} else if (state.memory !== '') {
				return {
					...state,
					num1: state.num1 + value,
					firstDisplay: state.firstDisplay + value,
					secondDisplay: ''
				}
			} else {
				return {
					...state,
					num1: state.num1 + value,
					firstDisplay: state.num1 + value,
					secondDisplay: ''
				}
			}
		} else if (state.operator !== '' && validator(value, /[0-9.]/)) {
			return {
				...state,
				num2: state.num2 + value,
				firstDisplay: state.firstDisplay + value,
				result: calculate(state.num1, state.num2 + value, state.operator).toString(),
				secondDisplay: calculate(state.num1, state.num2 + value, state.operator).toString()
			}
		}
	},
	[OPERATOR_VALUE]: (state, { value }) => {
		if (state.num1 === '') return state
		if (state.num1 !== '' && state.num2 === '' && validator(value, /[+÷×-]/)) {
			return {
				...state,
				operator: value,
				firstDisplay: state.firstDisplay + value,
				secondDisplay: ''
			}
		} else if (state.num2 !== '' && state.operator !== '' && state.result !== '' && validator(value, /[+÷×-]/)) {
			return {
				...state,
				firstDisplay: state.firstDisplay + value,
				secondDisplay: '',
				num1: state.result,
				result: calculate(state.num1, state.num2, state.operator).toString(),
				num2: '',
				operator: value
			}
		}
	},
	[OPERATOR_KEYS]: (state, { value }) => {
		switch (value) {
			case '+/-':
				if (state.num1 === '') return state
				if (state.num1 !== '' || state.num2 !== '') {
					if (state.num1 !== '' && state.num2 === '') {
						return {
							...state,
							num1: parseFloat(state.num1 * -1).toString(),
							firstDisplay: parseFloat(state.num1 * -1).toString()
						}
					} else {
						return {
							...state,
							num2: parseFloat(state.num2 * -1).toString(),
							firstDisplay: state.num1 + state.operator + parseFloat(state.num2 * -1).toString()
						}
					}
				}
				break
			case '%':
				if (state.num1 === '') return state
				if (state.num1 !== '' && state.num2 === '') {
					return {
						...state,
						num1: calculate(state.num1, state.num2, value).toString(),
						firstDisplay: calculate(state.num1, state.num2, value).toString()
					}
				} else {
					return {
						...state,
						num1: state.num1,
						firstDisplay: state.firstDisplay + value,
						num2: (calculate(state.num1, state.num2, value) * parseFloat(state.num2)).toString(),
						secondDisplay: calculate(
							state.num1,
							calculate(state.num1, state.num2, value) * parseFloat(state.num2),
							state.operator
						).toString()
					}
				}
			case 'm+':
				if (state.num1 === '') return state
				if ((state.mrc !== '' && state.num1 !== '') || state.num2 !== '') {
					if (state.num2 !== '') {
						return {
							...state,
							mrc: calculate(state.mrc, state.num2, '+').toString()
						}
					} else {
						return {
							...state,
							mrc: calculate(state.mrc, state.num1, '+').toString()
						}
					}
				} else if (state.result !== ' ' && state.mrc === '') {
					return {
						...state,
						mrc: state.result
					}
				} else {
					if (state.num2 !== '') {
						return {
							...state,
							mrc: state.num2
						}
					} else {
						return {
							...state,
							mrc: state.num1
						}
					}
				}
			case 'm-':
				if (state.num1 === '') return state
				if ((state.mrc !== '' && state.num1 !== '') || state.num2 !== '') {
					if (state.num2 !== '') {
						return {
							...state,
							mrc: calculate(state.mrc, state.num2, '-').toString()
						}
					} else {
						return {
							...state,
							mrc: calculate(state.mrc, state.num1, '-').toString()
						}
					}
				} else if (state.result !== ' ' && state.mrc === '') {
					return {
						...state,
						mrc: state.result
					}
				} else {
					if (state.num2 !== '') {
						return {
							...state,
							mrc: state.num2
						}
					} else {
						return {
							...state,
							mrc: state.num1
						}
					}
				}
			case 'mr':
				if (state.mrc !== '') {
					return {
						...state,
						num1: '',
						num2: '',
						operator: '',
						firstDisplay: state.mrc,
						secondDisplay: '',
						result: ''
					}
				} else {
					return
				}
			case 'mc':
				return {
					...state,
					mrc: ''
				}
			case '(':
				if (state.operator === '') {
					return {
						...state,
						firstDisplay: state.firstDisplay + '×' + value,
						num1: '',
						memory: state.num1
					}
				} else {
					return {
						...state,
						firstDisplay: state.firstDisplay + value,
						num1: '',
						operator: '',
						memory: state.num1,
						memoryOperator: state.operator
					}
				}
			case ')':
				if (state.memoryOperator === '') {
					return {
						...state,
						firstDisplay: state.firstDisplay + value,
						num1: state.memory,
						num2: state.result,
						operator: '×',
						result: calculate(state.memory, state.result, '×').toString(),
						secondDisplay: calculate(state.memory, state.result, '×').toString()
					}
				} else {
					return {
						...state,
						firstDisplay: state.firstDisplay + value,
						num1: state.memory,
						num2: state.result,
						operator: state.memoryOperator,
						result: calculate(state.memory, state.result, state.memoryOperator).toString(),
						secondDisplay: calculate(state.memory, state.result, state.memoryOperator).toString()
					}
				}
			case 'π':
				return {
					...state,
					firstDisplay: OPERATOR_TYPES.PI
				}
			case 'e':
				return {
					...state,
					firstDisplay: OPERATOR_TYPES.E
				}
			default:
				return state
		}
	},
	[CLEAR_BTN]: state => ({
		...state,
		firstDisplay: '',
		secondDisplay: '0',
		num1: '',
		num2: '',
		operator: '',
		result: '',
		mrc: state.mrc,
		memory: '',
		memoryOperator: ''
	}),
	[RESULT_VALUE]: state => {
		if (state.num1 === '') return state
		return {
			...state,
			firstDisplay: calculate(state.num1, state.num2, state.operator).toString(),
			secondDisplay: '',
			num1: calculate(state.num1, state.num2, state.operator).toString(),
			num2: '',
			operator: '',
			memory: '',
			memoryOperator: '',
			result: calculate(state.num1, state.num2, state.operator).toString()
		}
	},
	DEFAULT: state => state
}

export const calcReducer = (state, action) => {
	const handler = handlers[action.type] || handlers.DEFAULT
	return handler(state, action)
}
