import { useReducer } from 'react'
import { CalcContext } from './context'
import { calcReducer } from './reducer'
import { BTN_VALUE, CLEAR_BTN, RESULT_VALUE, OPERATOR_VALUE, OPERATOR_KEYS } from './types'

export const CalcState = ({ children }) => {
	const initialState = {
		firstDisplay: '',
		secondDisplay: '0',
		num1: '',
		num2: '',
		operator: '',
		result: '',
		mrc: '',
		memory: '',
		memoryOperator: ''
	}
	const [state, dispatch] = useReducer(calcReducer, initialState)

	const btnValue = value => dispatch({ type: BTN_VALUE, value })

	const resultValue = () => dispatch({ type: RESULT_VALUE })

	const operatorValue = value => dispatch({ type: OPERATOR_VALUE, value })

	const clearBtn = () => dispatch({ type: CLEAR_BTN })

	const operatorKeysValue = value => dispatch({ type: OPERATOR_KEYS, value })

	return (
		<CalcContext.Provider
			value={{
				...state,
				btnValue,
				clearBtn,
				resultValue,
				operatorValue,
				operatorKeysValue
			}}
		>
			{children}
		</CalcContext.Provider>
	)
}
