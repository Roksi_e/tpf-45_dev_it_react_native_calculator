import { useContext } from 'react'
import { StyleSheet, Text } from 'react-native'
import { CalcContext } from '../context/context'
import { AppDisplay } from './ui/AppDisplay'
import { THEME } from '../theme'

export const Display = () => {
	const { firstDisplay, secondDisplay } = useContext(CalcContext)

	const display2 = () => {
		if (firstDisplay !== '' && secondDisplay.length < 8) {
			return <Text style={{ ...styles.input2, fontSize: 40 }}>{secondDisplay}</Text>
		} else if (secondDisplay.length >= 8) {
			return <Text style={{ ...styles.input2, fontSize: 30 }}>{secondDisplay}</Text>
		} else {
			return <Text style={{ ...styles.input2, fontSize: 80 }}>{secondDisplay}</Text>
		}
	}

	const display1 = () => {
		if (firstDisplay !== '' && firstDisplay.length < 6) {
			return <Text style={{ ...styles.input1, fontSize: 60 }}>{firstDisplay}</Text>
		} else if (firstDisplay.length >= 6 && firstDisplay.length < 10) {
			return <Text style={{ ...styles.input1, fontSize: 50 }}>{firstDisplay}</Text>
		} else {
			return <Text style={{ ...styles.input1, fontSize: 40 }}>{firstDisplay}</Text>
		}
	}

	return (
		<AppDisplay style={styles.block}>
			{display1()}
			{display2()}
		</AppDisplay>
	)
}

const styles = StyleSheet.create({
	block: {
		paddingHorizontal: 20,
		paddingTop: 80,
		paddingBottom: 20
	},
	input1: {
		fontWeight: '300',
		textAlign: 'right',
		color: THEME.TEXT_COLOR
	},
	input2: {
		fontWeight: '300',
		textAlign: 'right',
		color: 'white'
	}
})
