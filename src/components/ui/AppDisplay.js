import { View, StyleSheet } from 'react-native'

export const AppDisplay = props => {
	return <View style={{ ...styles.block, ...props.style }}>{props.children}</View>
}

const styles = StyleSheet.create({
	block: {
		flex: 0.6,
		justifyContent: 'space-between',
		alignItems: 'flex-end'
	}
})
