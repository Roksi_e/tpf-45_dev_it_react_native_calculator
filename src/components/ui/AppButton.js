import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { THEME } from '../../theme'

export const AppButton = props => {
	return (
		<TouchableOpacity onPress={props.onPress} activeOpacity={0.7}>
			<View style={{ ...styles.default, backgroundColor: props.color, ...props.style }}>
				<Text
					style={[
						props.color === THEME.DARK_BTN ? styles.textLand : styles.text,
						{ color: props.color === THEME.GREY_BTN ? THEME.MAIN_COLOR : THEME.WHITE_COLOR }
					]}
				>
					{props.title}
				</Text>
			</View>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	default: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 50
	},
	text: {
		fontSize: 30,
		fontWeight: '400'
	},
	textLand: {
		fontSize: 15
	}
})
