import React, { useContext } from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import { StatusBar } from 'expo-status-bar'
import { AppButton } from './ui/AppButton'
import { THEME } from '../theme'
import { CalcContext } from '../context/context'

export const Buttons = () => {
	const { btnValue, clearBtn, resultValue, operatorValue, operatorKeysValue } = useContext(CalcContext)

	const width = Dimensions.get('window').width / 4 - THEME.MARGIN_HORIZONTAL * 2
	const widthZero = Dimensions.get('window').width / 2 - THEME.MARGIN_HORIZONTAL * 2
	const height = Dimensions.get('window').height / 8 - THEME.MARGIN_VERTICAL * 2

	return (
		<View style={{ width: Dimensions.get('window').width }}>
			<View style={styles.block}>
				<AppButton
					title="AC"
					onPress={() => clearBtn('AC')}
					color={THEME.GREY_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="+/-"
					onPress={() => operatorKeysValue('+/-')}
					color={THEME.GREY_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="%"
					onPress={() => operatorKeysValue('%')}
					color={THEME.GREY_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="÷"
					onPress={() => operatorValue('÷')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="7"
					onPress={() => btnValue('7')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="8"
					onPress={() => btnValue('8')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="9"
					onPress={() => btnValue('9')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="×"
					onPress={() => operatorValue('×')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="4"
					onPress={() => btnValue('4')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="5"
					onPress={() => btnValue('5')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="6"
					onPress={() => btnValue('6')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="-"
					onPress={() => operatorValue('-')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="1"
					onPress={() => btnValue('1')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="2"
					onPress={() => btnValue('2')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="3"
					onPress={() => btnValue('3')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="+"
					onPress={() => operatorValue('+')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="0"
					onPress={() => btnValue('0')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: widthZero, height: height }}
				/>
				<AppButton
					title="."
					onPress={() => btnValue('.')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
				<AppButton
					title="="
					onPress={() => resultValue('=')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.buttonPort, width: width, height: height }}
				/>
			</View>

			<StatusBar style="dark" />
		</View>
	)
}

const styles = StyleSheet.create({
	block: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	buttonPort: {
		marginHorizontal: THEME.MARGIN_HORIZONTAL,
		marginVertical: THEME.MARGIN_VERTICAL
	}
})
