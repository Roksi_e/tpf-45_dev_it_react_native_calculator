import { View, StyleSheet, Dimensions } from 'react-native'
import { StatusBar } from 'expo-status-bar'
import { AppButton } from './ui/AppButton'
import { THEME } from '../theme'
import React, { useContext } from 'react'
import { CalcContext } from '../context/context'

export const LandButtons = () => {
	const { btnValue, clearBtn, resultValue, operatorValue, operatorKeysValue } = useContext(CalcContext)

	const width = Dimensions.get('window').width / 10 - THEME.MARGIN_HORIZONTAL_LAND * 2
	const widthZero = Dimensions.get('window').width / 5 - THEME.MARGIN_HORIZONTAL_LAND * 2
	const height = Dimensions.get('window').height / 8 - THEME.MARGIN_VERTICAL_LAND * 2

	return (
		<View style={{ flex: 0.6, width: Dimensions.get('window').width }}>
			<View style={styles.block}>
				<AppButton
					title="("
					onPress={() => operatorKeysValue('(')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title=")"
					onPress={() => operatorKeysValue(')')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="mc"
					onPress={() => operatorKeysValue('mc')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="m+"
					onPress={() => operatorKeysValue('m+')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="m-"
					onPress={() => operatorKeysValue('m-')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="mr"
					onPress={() => operatorKeysValue('mr')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="C"
					onPress={() => clearBtn('C')}
					color={THEME.GREY_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="+/-"
					onPress={() => operatorKeysValue('+/-')}
					color={THEME.GREY_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="%"
					onPress={() => operatorKeysValue('%')}
					color={THEME.GREY_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="÷"
					onPress={() => operatorValue('÷')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="2ⁿᵈ"
					onPress={() => operatorKeysValue('2ⁿᵈ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="x²"
					onPress={() => operatorKeysValue('x²')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="x³"
					onPress={() => operatorKeysValue('x³')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="xᵞ"
					onPress={() => operatorKeysValue('xᵞ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="eᵡ"
					onPress={() => operatorKeysValue('eᵡ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="10ᵡ"
					onPress={() => operatorKeysValue('10ᵡ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="7"
					onPress={() => btnValue('7')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="8"
					onPress={() => btnValue('8')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="9"
					onPress={() => btnValue('9')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="×"
					onPress={() => operatorValue('×')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="⅟ₓ"
					onPress={() => operatorKeysValue('⅟ₓ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="²√ₓ"
					onPress={() => operatorKeysValue('²√ₓ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="³√ₓ"
					onPress={() => operatorKeysValue('³√ₓ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="ᵞ√ₓ"
					onPress={() => operatorKeysValue('ᵞ√ₓ')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="ln"
					onPress={() => operatorKeysValue('ln')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="log₁₀"
					onPress={() => operatorKeysValue('log₁₀')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="4"
					onPress={() => btnValue('4')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="5"
					onPress={() => btnValue('5')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="6"
					onPress={() => btnValue('6')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="-"
					onPress={() => operatorValue('-')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="x!"
					onPress={() => operatorKeysValue('x!')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="sin"
					onPress={() => operatorKeysValue('sin')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="cos"
					onPress={() => operatorKeysValue('cos')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="tan"
					onPress={() => operatorKeysValue('tan')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="e"
					onPress={() => operatorKeysValue('e')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="EE"
					onPress={() => operatorKeysValue('EE')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="1"
					onPress={() => btnValue('1')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="2"
					onPress={() => btnValue('2')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="3"
					onPress={() => btnValue('3')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="+"
					onPress={() => operatorValue('+')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
			</View>
			<View style={styles.block}>
				<AppButton
					title="Rad"
					onPress={() => operatorKeysValue('Rad')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="sinh"
					onPress={() => operatorKeysValue('sinh')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="cosh"
					onPress={() => operatorKeysValue('cosh')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="tanh"
					onPress={() => operatorKeysValue('tanh')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="π"
					onPress={() => operatorKeysValue('π')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="Rand"
					onPress={() => operatorKeysValue('Rand')}
					color={THEME.DARK_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="0"
					onPress={() => btnValue('0')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: widthZero, height: height }}
				/>
				<AppButton
					title="."
					onPress={() => btnValue('.')}
					color={THEME.GRAPHITE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
				<AppButton
					title="="
					onPress={() => resultValue('=')}
					color={THEME.ORANGE_BTN}
					style={{ ...styles.button, width: width, height: height }}
				/>
			</View>

			<StatusBar style="dark" />
		</View>
	)
}

const styles = StyleSheet.create({
	block: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	button: {
		marginHorizontal: THEME.MARGIN_HORIZONTAL_LAND,
		marginVertical: THEME.MARGIN_VERTICAL_LAND
	}
})
