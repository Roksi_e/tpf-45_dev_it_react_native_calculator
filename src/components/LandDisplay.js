import { useContext } from 'react'
import { StyleSheet, Text } from 'react-native'
import { CalcContext } from '../context/context'
import { AppDisplay } from './ui/AppDisplay'
import { THEME } from '../theme'

export const LandDisplay = () => {
	const { firstDisplay, secondDisplay } = useContext(CalcContext)

	const display2 = () => {
		if (firstDisplay !== '') {
			return <Text style={{ ...styles.input2, fontSize: 20 }}>{secondDisplay}</Text>
		} else {
			return <Text style={{ ...styles.input2, fontSize: 50 }}>{secondDisplay}</Text>
		}
	}

	return (
		<AppDisplay style={styles.block}>
			<Text style={styles.input1}>{firstDisplay}</Text>
			{display2()}
		</AppDisplay>
	)
}

const styles = StyleSheet.create({
	block: {
		flex: 0.2,
		justifyContent: 'center',
		paddingHorizontal: 20,
		paddingVertical: 3,
		marginBottom: 10
	},
	input1: {
		fontSize: 30,
		fontWeight: '300',
		textAlign: 'right',
		color: THEME.TEXT_COLOR
	},
	input2: {
		fontWeight: '300',
		textAlign: 'right',
		color: 'white'
	}
})
