import React, { useContext, useState, useEffect } from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import { Display } from '../components/Display'
import { Buttons } from '../components/Buttons'
import { THEME } from '../theme'
import { CalcContext } from '../context/context'
import { StatusBar } from 'expo-status-bar'
import { LandButtons } from '../components/LandButtons'
import { LandDisplay } from '../components/LandDisplay'

export const MainScreen = () => {
	const myContext = useContext(CalcContext)
	console.log(myContext)

	const [deviceSize, setDeviceSize] = useState({
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height
	})
	const [orientation, setOrientation] = useState({ orientation: 'portrait' })

	useEffect(() => {
		const update = () => {
			const { width, height } = Dimensions.get('window')
			let orientation = width > height ? 'landscape' : 'portrait'
			setOrientation({ orientation: orientation })
			setDeviceSize({ width: width, height: height })
		}

		const subscription = Dimensions.addEventListener('change', update)

		return () => subscription?.remove()
	})

	let content = () => {
		if (orientation.orientation === 'portrait') {
			return (
				<View style={{ ...styles.calc, width: deviceSize.width, height: deviceSize.height }}>
					<Display />
					<Buttons />
				</View>
			)
		} else if (orientation.orientation === 'landscape') {
			return (
				<View style={{ ...styles.calc, width: deviceSize.width, height: deviceSize.height }}>
					<LandDisplay />
					<LandButtons />
				</View>
			)
		}
	}

	return (
		<View style={styles.container}>
			{content()}
			<StatusBar style="auto" />
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: THEME.MAIN_COLOR,
		alignItems: 'center',
		justifyContent: 'center'
	},
	calc: {
		flex: 1,
		alignItems: 'flex-end',
		justifyContent: 'center'
	}
})
